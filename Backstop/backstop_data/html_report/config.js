report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/level_Menu_items_0_document_0_phone6_7_8.png",
        "test": "../bitmaps_test/20190626-153049/level_Menu_items_0_document_0_phone6_7_8.png",
        "selector": "document",
        "fileName": "level_Menu_items_0_document_0_phone6_7_8.png",
        "label": "Menu items",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://levelmusic.com/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone6/7/8",
        "engineErrorMsg": "waiting for selector \"[class=hp-container]\" failed: timeout 30000ms exceeded",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": -143,
            "height": -487
          },
          "misMatchPercentage": "16.70",
          "analysisTime": 88
        },
        "diffImage": "../bitmaps_test/20190626-153049/failed_diff_level_Menu_items_0_document_0_phone6_7_8.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/level_Menu_items_0_document_1_desktop1440.png",
        "test": "../bitmaps_test/20190626-153049/level_Menu_items_0_document_1_desktop1440.png",
        "selector": "document",
        "fileName": "level_Menu_items_0_document_1_desktop1440.png",
        "label": "Menu items",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://levelmusic.com/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop1440",
        "engineErrorMsg": "waiting for selector \"[class=hp-container]\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /Users/nkopychina/Documents/Work/Screen_comparison_backstop/backstop_data/bitmaps_reference/level_Menu_items_0_document_1_desktop1440.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/level_Menu_items_0_document_2_desktop1200.png",
        "test": "../bitmaps_test/20190626-153049/level_Menu_items_0_document_2_desktop1200.png",
        "selector": "document",
        "fileName": "level_Menu_items_0_document_2_desktop1200.png",
        "label": "Menu items",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://levelmusic.com/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop1200",
        "engineErrorMsg": "waiting for selector \"[class=hp-container]\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /Users/nkopychina/Documents/Work/Screen_comparison_backstop/backstop_data/bitmaps_reference/level_Menu_items_0_document_2_desktop1200.png"
      },
      "status": "fail"
    }
  ],
  "id": "level"
});