module.exports = async (page, scenario, vp) => {
  console.log('SCENARIO > ' + scenario.label);
  await require('./clickAndHoverHelper')(page, scenario);

  // Example: changing behavior based on config values
  if (vp.label.includes('phone')) {
    delay: 12000
    clickSelector: "#icons";
    delay: 12000
  }
};
